package com.dilshodjon216.notesapp.listeners;

import com.dilshodjon216.notesapp.entites.Note;

public interface NotesListener {
    void onNoteClicked(Note note,int positions);
}
